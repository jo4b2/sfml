#include <SFML/Graphics.hpp>
using namespace sf;
#define BLOC 32
#include "scene2.hpp"
#define NB_BLOC_LARGEUR 40
#define NB_BLOC_HAUTEUR 30
#define LARGEUR_FENETRE 1280
#define HAUTEUR_FENETRE 960
#define VITESSE 10
#define FPS 60

typedef struct
{
    int x;
    int y;
} Position;

void niveau2()
{
    //on cr�� la fen�tre
    RenderWindow fenetre(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Niveau 2");
    fenetre.setFramerateLimit(FPS);

    //on cr�� notre map
    int carte[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR] =
    {
        {1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,  4,  4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,  4,  4, 4, 4, 4, 1},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 17, 17, 17, 17, 3,  24,  25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 17, 17, 17, 17, 17, 17, 17, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18,  26,  27, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5,  2,  2, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5,  1,  1, 1, 12, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5,  1,  1, 1, 1, 2, 12, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5,  1,  1, 1, 1, 1, 3, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5,  1,  1, 1, 1, 1, 1, 2, 2, 12, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5,  1,  1, 1, 1, 1, 1, 1, 1, 3, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 16, 0, 0, 0, 0, 10,  4,  4, 4, 4, 4, 4, 4, 4, 4, 17, 17, 16,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 9, 2, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 9, 1, 1, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 9, 2, 2, 2, 2, 2,  2,  2, 1, 1, 1, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 9, 2, 1, 1, 1, 1, 1, 1,  1,  1, 1, 1, 1, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 5, 1, 1, 1, 1, 1, 1, 1,  1,  1, 1, 1, 1, 5},
        {3, 0, 14, 8, 8, 8, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  9, 2, 2, 4, 4, 4, 4, 4, 4, 4, 4,  4,  4, 4, 4, 4, 5},
        {3, 0, 18, 8, 8, 8, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9,  17,  4, 4, 11, 0, 0, 0, 0, 0, 0, 7, 7,  7,  0, 0, 0, 0, 5},
        {3, 17, 4, 17, 17, 17, 4, 17, 16, 0, 0, 0, 0, 0, 15, 17, 17, 17, 17, 17, 17, 11,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7,  7,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7,  7,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7,  7,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7,  7,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7,  7,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6,  6,  0, 0, 0, 0, 5},
        {1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,  2,  2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,  2,  2, 2, 2, 2, 1}
    };

    //on passe le tableau � deux dimensions en un tableau � une seule dimension
    int dessin[NB_BLOC_HAUTEUR*NB_BLOC_LARGEUR];
    int numeroBloc=0;
    for (int i=0; i<NB_BLOC_HAUTEUR; i++)
    {
        for(int j=0; j<NB_BLOC_LARGEUR; j++)
        {
            dessin[numeroBloc] = carte[i][j];
            numeroBloc++;
        }
    }

    //on d�finit le num�ro du bloc dans notre image tileset.png puis on cr��e le bloc image
    int positionXduBlocImage = 0;

    //on applique une texture
    Texture tileset[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR];
    if (!tileset[0][0].loadFromFile("tileset.png",IntRect(positionXduBlocImage, 0, BLOC, BLOC))) //positionBlocImage repr�sente le bloc que l'on veut dans l'image
        printf("Erreur de chargement de l'image tileset");

    Sprite blocTileset[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR];
    for (int y=0; y<NB_BLOC_HAUTEUR; y++)
    {
        for (int x=0; x<NB_BLOC_LARGEUR; x++)
        {
            numeroBloc = (y*NB_BLOC_LARGEUR + x);
            positionXduBlocImage = dessin[numeroBloc] * BLOC;
            tileset[y][x].loadFromFile("tileset.png",IntRect(positionXduBlocImage, 0, BLOC, BLOC));
            blocTileset[y][x].setTexture(tileset[y][x]);
            blocTileset[y][x].setPosition(x*BLOC, y*BLOC);
        }
    }

    //on cr�� le personnage
    Position posPerso;
    posPerso.x = BLOC;
    posPerso.y = HAUTEUR_FENETRE - 3*BLOC;
    RectangleShape perso(Vector2f(BLOC,2*BLOC));
    perso.setFillColor(Color::Blue);

    //on dessine les �l�ments exterieurs
    RectangleShape bloc1(Vector2f(2*BLOC, 3*BLOC));
    bloc1.setFillColor(Color(207,207,207));
    bloc1.setPosition(BLOC*28, BLOC*26);
    RectangleShape plateforme1(Vector2f(2*BLOC, 3*BLOC));
    plateforme1.setFillColor(Color(207,207,207));
    plateforme1.setPosition(BLOC*17, BLOC*20);
    RectangleShape plateforme2(Vector2f(2*BLOC, 2*BLOC));
    plateforme2.setFillColor(Color(207,207,207));
    plateforme2.setPosition(BLOC*10, BLOC*12);



    //on dessine les portes
    RectangleShape rect1(Vector2f(5*BLOC, BLOC));
    rect1.setFillColor(Color::Red);
    //rect1.setPosition(14*BLOC, 26*BLOC);
    RectangleShape rect2(Vector2f(5*BLOC, BLOC));
    rect2.setFillColor(Color(163,73,164));
    //rect2.setPosition(21*BLOC, 11*BLOC);
    RectangleShape rect3(Vector2f(4*BLOC, BLOC));
    rect3.setFillColor(Color::Yellow);
    //rect3.setPosition(19*BLOC, 6*BLOC);

    //on dessine les boutons
    RectangleShape bouton11(Vector2f(BLOC, BLOC));
    bouton11.setFillColor(Color::Red);
    bouton11.setPosition(36*BLOC, 27*BLOC);
    RectangleShape bouton12(Vector2f(BLOC, BLOC));
    bouton12.setFillColor(Color::Red);
    bouton12.setPosition(15*BLOC, 21*BLOC);
    RectangleShape bouton2(Vector2f(BLOC, BLOC));
    bouton2.setFillColor(Color(163,73,164));
    bouton2.setPosition(5*BLOC, 5*BLOC);
    RectangleShape bouton3(Vector2f(BLOC, BLOC));
    bouton3.setFillColor(Color::Yellow);
    bouton3.setPosition(4*BLOC, 22*BLOC);

    //d�but du jeu
    while (fenetre.isOpen())
    {
        Position porte1;
        porte1.x = 9 * BLOC;
        porte1.y = 23 * BLOC;
        Position porte2;
        porte2.x = 34*BLOC;
        porte2.y = 14*BLOC;
        Position porte3;
        porte3.x = 17*BLOC;
        porte3.y =14*BLOC;

            Event event;
            while (fenetre.pollEvent(event))
            {

                switch (event.type)
                {
                case Event::Closed:
                    fenetre.close();
                    break;
                }


                //on d�finit la position du perso en bloc
                Position blocPerso;
                blocPerso.x = posPerso.x / NB_BLOC_LARGEUR+2;
                blocPerso.y = posPerso.y / NB_BLOC_HAUTEUR;


                if (Keyboard::isKeyPressed(Keyboard::Escape))
                    fenetre.close();
                if (Keyboard::isKeyPressed(Keyboard::Up))
                    posPerso.y-=VITESSE;
                if (Keyboard::isKeyPressed(Keyboard::Down))
                    posPerso.y+=VITESSE;
                if (Keyboard::isKeyPressed(Keyboard::Left))
                    posPerso.x-=VITESSE;
                if (Keyboard::isKeyPressed(Keyboard::Right))
                {

                    posPerso.x+=VITESSE;
                    printf("%i %i", blocPerso.x, blocPerso.y);


                }

            }


        //on dessine la map
        for (int k=0; k<NB_BLOC_HAUTEUR; k++)
        {
            for (int l=0; l<NB_BLOC_LARGEUR; l++)
            {
                fenetre.draw(blocTileset[k][l]);
            }
        }
        //perso
        FloatRect boxperso = perso.getGlobalBounds();

        //bouton
        FloatRect boxbutton11 = bouton11.getGlobalBounds();
        FloatRect boxbutton12 = bouton12.getGlobalBounds();
        FloatRect boxbutton2 = bouton2.getGlobalBounds();
        FloatRect boxbutton3 = bouton3.getGlobalBounds();

        if (boxperso.intersects(boxbutton11) || boxperso.intersects(boxbutton12))
            porte1.x = porte1.x - 5* BLOC;
        if (boxperso.intersects(boxbutton2))
            porte2.x = porte2.x - 5* BLOC;
        if (boxperso.intersects(boxbutton3))
            porte3.x = porte3.x - 4* BLOC;


        //on place le perso
        perso.setPosition(posPerso.x,posPerso.y);

        rect1.setPosition(porte1.x, porte1.y);
        rect2.setPosition(porte2.x, porte2.y);
        rect3.setPosition(porte3.x, porte3.y);
        fenetre.draw(bloc1);
        fenetre.draw(plateforme1);
        fenetre.draw(plateforme2);
        fenetre.draw(rect1);
        fenetre.draw(rect2);
        fenetre.draw(rect3);
        fenetre.draw(bouton11);
        fenetre.draw(bouton12);
        fenetre.draw(bouton2);
        fenetre.draw(bouton3);
        fenetre.draw(perso);

        fenetre.display();
    }

}




