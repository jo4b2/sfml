#include <SFML/Graphics.hpp>
using namespace sf;
#define BLOC 32
#define NB_BLOC_LARGEUR 40
#define NB_BLOC_HAUTEUR 30
#define LARGEUR_FENETRE 1280
#define HAUTEUR_FENETRE 960
#define VITESSE 10
#define FPS 60
#include "scene2.hpp"

typedef struct
{
    int x;
    int y;
} Position;

void niveau6()
{
    //on cr�� la fen�tre
    RenderWindow fenetre(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Niveau 1");
    fenetre.setFramerateLimit(FPS);

    //on cr�� notre map
    int carte[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR] =
    {
        {1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,  4,  4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,  4,   4,  4,  4, 4, 1},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 15, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 12, 0,  0,  9, 16, 0, 0, 9, 17, 17, 17, 17, 17, 17,  16,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13, 0,  0,  18, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  15,  17, 17, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 15, 17, 2, 17, 16, 0, 0, 0, 0, 0, 9, 17, 16, 0, 0, 0,  0,  18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0,  0,  10, 16, 0, 0, 0, 0, 0, 0, 0, 15, 17,  17,   17,  17,  17, 17, 5},
        {3, 17, 16, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 15, 17, 3, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,   0,  15,  17, 17, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 0,  0,   0,  0,  0, 0, 5},
        {3, 17, 16, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 15, 17, 3, 0, 0, 0, 0, 0,  0,  0, 0, 0, 7, 0, 0, 0, 0, 0, 18, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0,  0,  0, 0, 7, 7, 0, 0, 0, 0, 0, 18, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 18, 7, 7, 0, 0, 0,  0,  0, 0, 6, 6, 0, 0, 0, 0, 0, 18, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 18, 7, 7, 0, 0, 7,  7,  0, 0, 15, 16, 0, 0, 0, 0, 0, 18, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 18, 6, 6, 0, 0, 7,  7,  0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 0,  0,   0,  0,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 5, 17, 16, 0, 0, 6,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 0,  0,   0,  0,  0, 0, 5},
        {3, 17, 16, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 15, 17, 3, 0, 0, 0, 0, 15,  16,  0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 0,  0,   24,  25,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 18, 8, 8, 8, 8, 8,  8,  8, 8, 8, 8, 8, 8, 8, 8, 8, 18, 0,  0,   26,  27,  0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 18, 8, 8, 8, 8, 8,  8,  8, 8, 8, 8, 8, 8, 8, 8, 8, 18, 0,  0,   0,  0,  0, 0, 5},
        {1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,  2,  2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,  2,   2,  2,  2, 2, 1}
    };

    //on passe le tableau � deux dimensions en un tableau � une seule dimension
    int dessin[NB_BLOC_HAUTEUR*NB_BLOC_LARGEUR];
    int numeroBloc=0;
    for (int i=0; i<NB_BLOC_HAUTEUR; i++)
    {
        for(int j=0; j<NB_BLOC_LARGEUR; j++)
        {
            dessin[numeroBloc] = carte[i][j];
            numeroBloc++;
        }
    }

    //on d�finit le num�ro du bloc dans notre image tileset.png puis on cr��e le bloc image
    int positionXduBlocImage = 0;

    //on applique une texture
    Texture tileset[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR];
    if (!tileset[0][0].loadFromFile("tileset.png",IntRect(positionXduBlocImage, 0, BLOC, BLOC))) //positionBlocImage repr�sente le bloc que l'on veut dans l'image
        printf("Erreur de chargement de l'image tileset");

    Sprite blocTileset[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR];
    for (int y=0; y<NB_BLOC_HAUTEUR; y++)
    {
        for (int x=0; x<NB_BLOC_LARGEUR; x++)
        {
            numeroBloc = (y*NB_BLOC_LARGEUR + x);
            positionXduBlocImage = dessin[numeroBloc] * BLOC;
            tileset[y][x].loadFromFile("tileset.png",IntRect(positionXduBlocImage, 0, BLOC, BLOC));
            blocTileset[y][x].setTexture(tileset[y][x]);
            blocTileset[y][x].setPosition(x*BLOC, y*BLOC);
        }
    }

    //on cr�� le personnage
    Position posPerso;
    posPerso.x = BLOC;
    posPerso.y = HAUTEUR_FENETRE - 3*BLOC;
    RectangleShape perso(Vector2f(BLOC,2*BLOC));
    perso.setFillColor(Color::Blue);

    //on dessine les �l�ments exterieurs
    RectangleShape bloc1(Vector2f(2*BLOC, 2*BLOC));
    bloc1.setFillColor(Color(207,207,207));
    bloc1.setPosition(BLOC*23, BLOC*4);
    RectangleShape bloc4(Vector2f(2*BLOC, BLOC));
    bloc4.setFillColor(Color(207,207,207));
    bloc4.setPosition(BLOC*10, BLOC*5);
    RectangleShape bloc5(Vector2f(2*BLOC, BLOC));
    bloc5.setFillColor(Color(207,207,207));
    bloc5.setPosition(BLOC*15, BLOC*5);

    ConvexShape bloc2(7);
    bloc2.setPoint (0, Vector2f (0, 0));
    bloc2.setPoint (1, Vector2f (0, BLOC));
    bloc2.setPoint (2, Vector2f (BLOC, BLOC));
    bloc2.setPoint (3, Vector2f (BLOC, -BLOC));
    bloc2.setPoint (4, Vector2f (2*BLOC, -BLOC));
    bloc2.setPoint (5, Vector2f (2*BLOC, 1*BLOC));
    bloc2.setPoint (6, Vector2f (1*BLOC, 1*BLOC));
    bloc2.setFillColor(Color(207,207,207));
    bloc2.setPosition (27*BLOC,5*BLOC);

    ConvexShape bloc3(6);
    bloc3.setPoint (0, Vector2f (0, 0));
    bloc3.setPoint (1, Vector2f (0, -2*BLOC));
    bloc3.setPoint (2, Vector2f (2*BLOC, -2*BLOC));
    bloc3.setPoint (3, Vector2f (2*BLOC, -BLOC));
    bloc3.setPoint (4, Vector2f (BLOC, -BLOC));
    bloc3.setPoint (5, Vector2f (BLOC,0));
    bloc3.setFillColor(Color(207,207,207));
    bloc3.setPosition (31*BLOC,6*BLOC);


    //on dessine les portes
    RectangleShape rect1(Vector2f(3*BLOC, BLOC));
    rect1.setFillColor(Color::Red);
    RectangleShape rect2(Vector2f(3*BLOC, BLOC));
    rect2.setFillColor(Color(163,73,164));
    RectangleShape rect3(Vector2f(BLOC, 5*BLOC));
    rect3.setFillColor(Color::Yellow);
    RectangleShape rect4(Vector2f(2*BLOC, BLOC));
    rect4.setFillColor(Color::Green);
    RectangleShape rect5(Vector2f(2*BLOC, BLOC));
    rect5.setFillColor(Color::Blue);
    RectangleShape rect6(Vector2f(BLOC, 2*BLOC));
    rect6.setFillColor(Color::Black);
    RectangleShape rect7(Vector2f(BLOC, 2*BLOC));
    rect7.setFillColor(Color(128,255,255));

    //on dessine les boutons
    RectangleShape bouton1(Vector2f(BLOC, BLOC));
    bouton1.setFillColor(Color::Red);
    bouton1.setPosition(15*BLOC, 24*BLOC);
    RectangleShape bouton2(Vector2f(BLOC, BLOC));
    bouton2.setFillColor(Color(163,73,164));
    bouton2.setPosition(BLOC, 12*BLOC);
    RectangleShape bouton3(Vector2f(BLOC, BLOC));
    bouton3.setFillColor(Color::Yellow);
    bouton3.setPosition(36*BLOC, 11*BLOC);
    RectangleShape bouton4(Vector2f(BLOC, BLOC));
    bouton4.setFillColor(Color::Green);
    bouton4.setPosition(6*BLOC, 4*BLOC);
    RectangleShape bouton5(Vector2f(BLOC, BLOC));
    bouton5.setFillColor(Color::Blue);
    bouton5.setPosition(34*BLOC, 4*BLOC);
    RectangleShape bouton6(Vector2f(BLOC, BLOC));
    bouton6.setFillColor(Color::Black);
    bouton6.setPosition(17*BLOC, 23*BLOC);
    RectangleShape bouton7(Vector2f(BLOC, BLOC));
    bouton7.setFillColor(Color(128,255,255));
    bouton7.setPosition(8*BLOC, 10*BLOC);
    RectangleShape bouton8(Vector2f(BLOC, BLOC));
    bouton8.setFillColor(Color::Yellow);
    bouton8.setPosition(37*BLOC, 16*BLOC);

    //d�but du jeu
    while (fenetre.isOpen())
    {
        Position porte1;
        porte1.x = 9*BLOC;
        porte1.y = 23 * BLOC;
        Position porte2;
        porte2.x = 5*BLOC;
        porte2.y = 17*BLOC;
        Position porte3;
        porte3.x = 32*BLOC;
        porte3.y =14*BLOC;
        Position porte4;
        porte4.x = 27*BLOC;
        porte4.y =6*BLOC;
        Position porte5;
        porte5.x = 19*BLOC;
        porte5.y =6*BLOC;
        Position porte6;
        porte6.x = 30*BLOC;
        porte6.y =22*BLOC;
        Position porte7;
        porte7.x = 32*BLOC;
        porte7.y =20*BLOC;



Event event;
        while (fenetre.pollEvent(event))
        {

            switch (event.type)
            {
            case Event::Closed:
                fenetre.close();
                break;
            }


            //on d�finit la position du perso en bloc
            Position blocPerso;
            blocPerso.x = posPerso.x / NB_BLOC_LARGEUR+2;
            blocPerso.y = posPerso.y / NB_BLOC_HAUTEUR;


            if (Keyboard::isKeyPressed(Keyboard::Escape))
                fenetre.close();
            if (Keyboard::isKeyPressed(Keyboard::Up))
                posPerso.y-=VITESSE;
            if (Keyboard::isKeyPressed(Keyboard::Down))
                posPerso.y+=VITESSE;
            if (Keyboard::isKeyPressed(Keyboard::Left))
                posPerso.x-=VITESSE;
            if (Keyboard::isKeyPressed(Keyboard::Right))
            {

                posPerso.x+=VITESSE;
                printf("%i %i", blocPerso.x, blocPerso.y);


            }

        }

        //on dessine la map
        for (int k=0; k<NB_BLOC_HAUTEUR; k++)
        {
            for (int l=0; l<NB_BLOC_LARGEUR; l++)
            {
                fenetre.draw(blocTileset[k][l]);
            }
        }
        //perso
        FloatRect boxperso = perso.getGlobalBounds();

        //bouton
        FloatRect boxbutton1 = bouton1.getGlobalBounds();
        FloatRect boxbutton2 = bouton2.getGlobalBounds();
        FloatRect boxbutton3 = bouton3.getGlobalBounds();
        FloatRect boxbutton4 = bouton4.getGlobalBounds();
        FloatRect boxbutton5 = bouton5.getGlobalBounds();
        FloatRect boxbutton6 = bouton6.getGlobalBounds();
        FloatRect boxbutton7 = bouton7.getGlobalBounds();
        FloatRect boxbutton8 = bouton8.getGlobalBounds();

        if (boxperso.intersects(boxbutton1))
            porte1.x = porte1.x - 4* BLOC;
        if (boxperso.intersects(boxbutton2))
            porte2.x = porte2.x + 4* BLOC;
        if (boxperso.intersects(boxbutton3))
            porte3.y = porte3.y - 5* BLOC;
        if (boxperso.intersects(boxbutton4))
            porte4.x = porte4.x - 2* BLOC;
        if (boxperso.intersects(boxbutton5))
            porte5.x = porte5.x + 2* BLOC;
        if (boxperso.intersects(boxbutton6))
            rect6.setRotation(90);
        if (boxperso.intersects(boxbutton7))
            rect7.setRotation(90);
        if (boxperso.intersects(boxbutton8))
            porte3.y = porte3.y - 5* BLOC;



        //on place et on dessine le perso
        perso.setPosition(posPerso.x,posPerso.y);

        rect1.setPosition(porte1.x, porte1.y);
        rect2.setPosition(porte2.x, porte2.y);
        rect3.setPosition(porte3.x, porte3.y);
        rect4.setPosition(porte4.x, porte4.y);
        rect5.setPosition(porte5.x, porte5.y);
        rect6.setPosition(porte6.x, porte6.y);
        rect7.setPosition(porte7.x, porte7.y);
        fenetre.draw(bloc1);
        fenetre.draw(bloc2);
        fenetre.draw(bloc3);
        fenetre.draw(bloc4);
        fenetre.draw(bloc5);
        fenetre.draw(rect1);
        fenetre.draw(rect2);
        fenetre.draw(rect3);
        fenetre.draw(rect4);
        fenetre.draw(rect5);
        fenetre.draw(rect6);
        fenetre.draw(rect7);
        fenetre.draw(bouton1);
        fenetre.draw(bouton2);
        fenetre.draw(bouton3);
        fenetre.draw(bouton4);
        fenetre.draw(bouton5);
        fenetre.draw(bouton6);
        fenetre.draw(bouton7);
        fenetre.draw(bouton8);
        fenetre.draw(perso);


        fenetre.display();
    }

}



