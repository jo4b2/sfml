#include <SFML/Graphics.hpp>
#include <time.h>
#include <iostream>
#include "scene2.hpp"
#define LARGEUR_FENETRE 1280
#define HAUTEUR_FENETRE 928
#define ATTENTE 1

using namespace sf;


void chargement()
{
    RenderWindow transition( VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "chargement");

    //INITIALISATION CHRONOMETRE///
    Clock clock;

    Time micro =  microseconds( 10000 );
    Time milli =  milliseconds( 10 );
    Time second =  seconds( 0.01f );

    std::cout << (micro.asSeconds( )) << std::endl;
    std::cout << (micro.asMilliseconds( )) << std::endl;
    std::cout << (micro.asMicroseconds( )) << std::endl;

    //INITIALISATION TEXTURE CHARGEMENT//
    Texture bgchargement;
    if (!bgchargement.loadFromFile("chargement.png"))
        printf("exit failure");
    Sprite bgChargement(bgchargement);

    //Tant que la fenetre est ouerte
    while (transition.isOpen())
    {

        Time elapsed = clock.getElapsedTime( ); //r�cup�re le temps �coul� depuis le demarrage de la clock

        //fermeture fenetre en fonction du temps d'attente
        if( elapsed.asSeconds() > ATTENTE)
            transition.close();



        // Process events
        Event event;
        while (transition.pollEvent(event))
        {
            // Close window : exit
            if (event.type ==  Event::Closed)
                transition.close();
        }

        // Clear screen
        transition.clear();

        // Affichage des sprites
        transition.draw(bgChargement);


        // Update the window
        transition.display();
    }

}
