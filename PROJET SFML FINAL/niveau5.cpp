#include <SFML/Graphics.hpp>
#include "scene2.hpp"
using namespace sf;
#define BLOC 32
#define NB_BLOC_LARGEUR 40
#define NB_BLOC_HAUTEUR 30
#define LARGEUR_FENETRE 1280
#define HAUTEUR_FENETRE 960
#define VITESSE 10
#define FPS 60

typedef struct
{
    int x;
    int y;
} Position;

void niveau5(bool*win5)

{
    *win5 = true;
    //on cr�� la fen�tre
    RenderWindow fenetreniv5(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Niveau 5");
    fenetreniv5.setFramerateLimit(FPS);

    //on cr�� notre map
    int carte[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR] =
    {
        {1,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,1},
        {3,0,0,0,0,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5},
        {3,0,0,0,0,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5},
        {3,0,0,0,0,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5},
        {3,0,0,0,0,6,6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5},
        {3,17,17,17,16,7,7,7,9,17,17,17,16,0,0,0,9,17,17,17,17,17,12,0,0,0,15,17,17,2,17,17,16,8,8,8,9,17,17,5},
        {3,0,0,0,0,7,7,7,18,0,0,0,0,0,0,0,18,0,0,0,0,0,18,0,0,0,0,0,0,18,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,7,7,7,18,0,0,0,0,0,0,0,18,0,0,0,0,0,18,0,0,0,0,0,0,18,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,7,7,7,18,0,0,0,0,0,0,0,18,24,25,0,0,0,18,0,0,0,0,0,0,18,0,0,0,0,0,0,13,0,0,5},
        {3,0,0,0,0,0,0,0,13,0,0,0,0,0,0,0,18,26,27,0,0,0,18,0,0,0,0,0,0,18,0,0,0,0,0,0,0,0,0,5},
        {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,17,16,0,0,0,18,0,0,0,0,0,0,10,17,17,17,17,17,17,12,0,0,5},
        {3,17,17,17,17,17,16,0,0,0,0,15,17,17,17,17,3,0,0,0,0,0,18,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,0,0,9,3,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,0,0,5,3,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,15,17,4,3,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,0,0,0,18,0,0,0,0,0,0,9,17,17,17,16,0,0,18,0,0,5},
        {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,12,0,0,0,0,18,0,0,0,0,0,0,18,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,3,0,0,0,0,18,0,0,0,0,0,0,18,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,15,17,17,2,17,17,17,17,17,17,17,4,4,17,16,0,0,18,0,0,0,0,0,0,18,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,0,0,0,18,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,0,0,0,0,18,0,0,0,0,0,0,13,0,0,5},
        {3,0,0,0,0,0,0,0,18,0,0,0,0,0,0,0,0,0,0,0,0,9,1,2,2,2,2,2,2,3,0,0,0,0,0,0,0,0,0,5},
        {3,0,0,0,0,0,0,0,13,0,0,0,0,0,0,0,0,0,0,0,0,5,1,1,1,1,1,1,1,3,0,0,0,0,0,0,14,0,0,5},
        {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,1,4,4,4,4,4,4,4,4,16,0,0,15,17,17,3,0,0,5},
        {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,0,0,0,14,0,0,0,0,0,0,0,0,15,4,4,4,11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,0,0,0,13,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,5},
        {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,5},
        {1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1}
    };

    //on passe le tableau � deux dimensions en un tableau � une seule dimension
    int dessin[NB_BLOC_HAUTEUR*NB_BLOC_LARGEUR];
    int numeroBloc=0;
    for (int i=0; i<NB_BLOC_HAUTEUR; i++)
    {
        for(int j=0; j<NB_BLOC_LARGEUR; j++)
        {
            dessin[numeroBloc] = carte[i][j];
            numeroBloc++;
        }
    }

    //on d�finit le num�ro du bloc dans notre image tileset.png puis on cr��e le bloc image
    int positionXduBlocImage = 0;

    //on applique une texture
    Texture tileset[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR];
    if (!tileset[0][0].loadFromFile("tileset.png",IntRect(positionXduBlocImage, 0, BLOC, BLOC))) //positionBlocImage repr�sente le bloc que l'on veut dans l'image
        printf("Erreur de chargement de l'image tileset");

    Sprite blocTileset[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR];
    for (int y=0; y<NB_BLOC_HAUTEUR; y++)
    {
        for (int x=0; x<NB_BLOC_LARGEUR; x++)
        {
            numeroBloc = (y*NB_BLOC_LARGEUR + x);
            positionXduBlocImage = dessin[numeroBloc] * BLOC;
            tileset[y][x].loadFromFile("tileset.png",IntRect(positionXduBlocImage, 0, BLOC, BLOC));
            blocTileset[y][x].setTexture(tileset[y][x]);
            blocTileset[y][x].setPosition(x*BLOC, y*BLOC);
        }
    }

    //on cr�� le personnage
    Position posPerso;
    posPerso.x = BLOC;
    posPerso.y = HAUTEUR_FENETRE - 3*BLOC;
    RectangleShape perso(Vector2f(BLOC,2*BLOC));
    perso.setFillColor(Color::Blue);

    //on dessine les �l�ments exterieurs
    RectangleShape bloc1(Vector2f(1*BLOC, 2*BLOC));
    bloc1.setFillColor(Color(207,207,207));
    bloc1.setPosition(BLOC*11, BLOC*9);


    //on dessine les portes
    RectangleShape rect1(Vector2f(BLOC, 3*BLOC));
    rect1.setFillColor(Color::Red);
    //rect1.setPosition(14*BLOC, 26*BLOC);
    RectangleShape rect2(Vector2f(BLOC, 4*BLOC));
    rect2.setFillColor(Color(163,73,164));
    //rect2.setPosition(21*BLOC, 11*BLOC);
    RectangleShape rect3(Vector2f(BLOC, 5*BLOC));
    rect3.setFillColor(Color::Yellow);

    RectangleShape rect4(Vector2f(2*BLOC,BLOC));
    rect4.setFillColor(Color::Green);
    //rect3.setPosition(19*BLOC, 6*BLOC);

    //on dessine les boutons et on les place
    RectangleShape bouton1(Vector2f(BLOC, BLOC));
    bouton1.setFillColor(Color::Red);
    bouton1.setPosition(30*BLOC, 21*BLOC);
    RectangleShape bouton2(Vector2f(BLOC, BLOC));
    bouton2.setFillColor(Color(163,73,164));
    bouton2.setPosition(14*BLOC, 16*BLOC);
    RectangleShape bouton3(Vector2f(BLOC, BLOC));
    bouton3.setFillColor(Color::Yellow);
    bouton3.setPosition(2*BLOC, 10*BLOC);
    RectangleShape bouton4(Vector2f(BLOC, BLOC));
    bouton4.setFillColor(Color::Green);
    bouton4.setPosition(32*BLOC, 14*BLOC);

    //d�but du jeu
    while (fenetreniv5.isOpen())
    {
        //Positionnement des portes en fonction des blocs
        Position porte1;
        porte1.x = 8*BLOC;
        porte1.y = 22 * BLOC;
        Position porte2;
        porte2.x = 29*BLOC;
        porte2.y = 11*BLOC;
        Position porte3;
        porte3.x = 12*BLOC;
        porte3.y =6*BLOC;
        Position porte4;
        porte4.x = 39*BLOC;
        porte4.y =21*BLOC;



Event event;
        while (fenetreniv5.pollEvent(event))
        {

            switch (event.type)
            {
            case Event::Closed:
                fenetreniv5.close();
                break;
            }


            //on d�finit la position du perso en bloc
            Position blocPerso;
            blocPerso.x = posPerso.x / NB_BLOC_LARGEUR+2;
            blocPerso.y = posPerso.y / NB_BLOC_HAUTEUR;


            if (Keyboard::isKeyPressed(Keyboard::Escape))
            {
                fenetreniv5.close();
                sceneSuivante();
            }
            //mouvements du perso
            if (Keyboard::isKeyPressed(Keyboard::Up))
                posPerso.y-=VITESSE;
            if (Keyboard::isKeyPressed(Keyboard::Down))
                posPerso.y+=VITESSE;
            if (Keyboard::isKeyPressed(Keyboard::Left))
                posPerso.x-=VITESSE;
            if (Keyboard::isKeyPressed(Keyboard::Right))
                posPerso.x+=VITESSE;

        }

        //on dessine la map
        for (int k=0; k<NB_BLOC_HAUTEUR; k++)
        {
            for (int l=0; l<NB_BLOC_LARGEUR; l++)
            {
                fenetreniv5.draw(blocTileset[k][l]);
            }
        }
        //perso
        FloatRect boxperso = perso.getGlobalBounds();

        //bouton
        FloatRect boxbutton1 = bouton1.getGlobalBounds();
        FloatRect boxbutton2 = bouton2.getGlobalBounds();
        FloatRect boxbutton3 = bouton3.getGlobalBounds();
        FloatRect boxbutton4 = bouton4.getGlobalBounds();

        //dimensions portes
        if (boxperso.intersects(boxbutton1))
            porte1.y = porte1.y - 3* BLOC;
        if (boxperso.intersects(boxbutton2))
            porte2.y = porte2.y + 4* BLOC;
        if (boxperso.intersects(boxbutton3))
            porte3.y = porte3.y - 5* BLOC;
        if (boxperso.intersects(boxbutton4))
            porte4.x = porte4.x - 2* BLOC;



        //on place et on dessine le perso
        perso.setPosition(posPerso.x,posPerso.y);

        //on lie les positions de la porte avec les rectangles shape
        rect1.setPosition(porte1.x, porte1.y);
        rect2.setPosition(porte2.x, porte2.y);
        rect3.setPosition(porte3.x, porte3.y);
        rect4.setPosition(porte4.x, porte4.y);


        fenetreniv5.draw(bloc1);
        fenetreniv5.draw(rect1);
        fenetreniv5.draw(rect2);
        fenetreniv5.draw(rect3);
        fenetreniv5.draw(rect4);
        fenetreniv5.draw(bouton1);
        fenetreniv5.draw(bouton2);
        fenetreniv5.draw(bouton3);
        fenetreniv5.draw(bouton4);
        fenetreniv5.draw(perso);


        fenetreniv5.display();
    }

}



