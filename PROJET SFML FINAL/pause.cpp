#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "scene2.hpp"
#define LARGEUR_FENETRE 1280
#define HAUTEUR_FENETRE 928
#define VOLUME 10

using namespace sf;

void scenePause()
{

    RenderWindow pause( VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "SFML window");

    SoundBuffer buffer2;
    if (!buffer2.loadFromFile("sons/clic.wav"))
        printf("exit failure");

    Sound effetClic;
    effetClic.setBuffer(buffer2);
    effetClic.setVolume(VOLUME-5);



    Texture bgpause;
    if (!bgpause.loadFromFile("menuPause.png"))
        printf("exit failure");
    Sprite bgPause(bgpause);

    Texture quitter;
    if (!quitter.loadFromFile("boutons/boutonQuitterPause.png"))
        printf("exit failure");
    Sprite btQuitter(quitter);
    btQuitter.setPosition(-2000,-2000);

    Texture reprendre;
    if (!reprendre.loadFromFile("boutons/boutonReprendrePause.png"))
        printf("exit failure");
    Sprite btReprendre(reprendre);
    btReprendre.setPosition(-2000,-2000);



    while (pause.isOpen())
    {
        Vector2i position = Mouse::getPosition(pause);

        if(position.x <= 377  && position.x >= 70 && position.y <= 652 && position.y>= 497)
            btReprendre.setPosition(70,497);
        else
            btReprendre.setPosition(-2000,-2000);

        if(position.x <= 377  && position.x >= 70 && position.y <= 909 && position.y>= 717)
            btQuitter.setPosition(70,717);
        else
            btQuitter.setPosition(-2000,-2000);

        // Process events
        Event event;
        while (pause.pollEvent(event))
        {
            //reprendre
            if(position.x <= 377  && position.x >= 70 && position.y <= 652 && position.y>= 497 && Mouse::isButtonPressed(Mouse::Left))
            {
                effetClic.play();
                pause.close();

            }
            //quitter
            if(position.x <= 377  && position.x >= 70 && position.y <= 909 && position.y>= 717 && Mouse::isButtonPressed(Mouse::Left))
            {
                effetClic.play();
                pause.close();
                main();
            }


            // Close window : exit
            if (event.type ==  Event::Closed)
                pause.close();
        }

        // Clear screen
        pause.clear();

        // Draw the sprite

        pause.draw(bgPause);
        pause.draw(btReprendre);
        pause.draw(btQuitter);

        // Update the window
        pause.display();
    }


}
