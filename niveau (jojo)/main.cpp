#include <SFML/Graphics.hpp>
using namespace sf;
#define BLOC 32
#define NB_BLOC_LARGEUR 40
#define NB_BLOC_HAUTEUR 30
#define LARGEUR_FENETRE 1280
#define HAUTEUR_FENETRE 960
#define VITESSE 8
#define FPS 60

typedef struct
{
    int x;
    int y;
} Position;

int main()
{
    //on cr�� la fen�tre
    RenderWindow fenetre(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Niveau 1");
    fenetre.setFramerateLimit(FPS);

    //on cr�� notre map
    int carte[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR] =
    {
        {1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,  4,  4, 4, 4, 4, 1},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 25,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 26, 27,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 15, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17,  17,  17, 17, 17, 16, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 1, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 1, 1, 2, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 1, 1, 1, 1, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 1, 1, 1, 1, 1, 2, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 1, 1, 1, 1, 1, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 4, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 1, 1, 1, 1, 1, 1, 1, 2, 12, 0, 0, 0, 0, 0, 0, 14, 8, 8, 8, 8, 8, 8, 8, 8, 8, 14, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0, 0, 0, 0, 0, 18, 8, 8, 8, 8, 8, 8, 8, 8, 8, 18, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 17, 17, 17, 17, 17, 17, 4, 17, 17, 17, 17, 17, 17, 17, 17, 17, 4, 17, 17, 17, 17, 17, 16, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0, 0, 0, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 9, 2, 12, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 5, 1, 3, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 5, 1, 3, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 17, 17, 17, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 5, 1, 3, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 5, 1, 3, 5},
        {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 5, 1, 3, 5},
        {1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,  2,  2, 2, 2, 2, 1}
    };

    //on passe le tableau � deux dimensions en un tableau � une seule dimension
    int dessin[NB_BLOC_HAUTEUR*NB_BLOC_LARGEUR];
    int numeroBloc=0;
    for (int i=0; i<NB_BLOC_HAUTEUR; i++)
    {
        for(int j=0; j<NB_BLOC_LARGEUR; j++)
        {
            dessin[numeroBloc] = carte[i][j];
            numeroBloc++;
        }
    }

    //on d�finit le num�ro du bloc dans notre image tileset.png puis on cr��e le bloc image
    int positionXduBlocImage = 0;

    //on applique une texture
    Texture tileset[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR];
    if (!tileset[0][0].loadFromFile("tileset.png",IntRect(positionXduBlocImage, 0, BLOC, BLOC))) //positionBlocImage repr�sente le bloc que l'on veut dans l'image
        printf("Erreur de chargement de l'image tileset");

    Sprite blocTileset[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR];
    for (int y=0; y<NB_BLOC_HAUTEUR; y++)
    {
        for (int x=0; x<NB_BLOC_LARGEUR; x++)
        {
            numeroBloc = (y*NB_BLOC_LARGEUR + x);
            positionXduBlocImage = dessin[numeroBloc] * BLOC;
            tileset[y][x].loadFromFile("tileset.png",IntRect(positionXduBlocImage, 0, BLOC, BLOC));
            blocTileset[y][x].setTexture(tileset[y][x]);
            blocTileset[y][x].setPosition(x*BLOC, y*BLOC);
        }
    }

    //on cr�� le personnage
    Position posPerso;
    posPerso.x = BLOC;
    posPerso.y = HAUTEUR_FENETRE - 3*BLOC + 4;
    RectangleShape perso(Vector2f(BLOC,2*BLOC-6));
    perso.setFillColor(Color::Blue);


    //on dessine les �l�ments exterieurs
    RectangleShape bloc1(Vector2f(2*BLOC, 3*BLOC));
    bloc1.setFillColor(Color(207,207,207));
    bloc1.setPosition(BLOC*26, BLOC*26);


    //on dessine les portes
    RectangleShape rect1(Vector2f(BLOC, 3*BLOC));
    rect1.setFillColor(Color::Red);
    //rect1.setPosition(14*BLOC, 26*BLOC);
    RectangleShape rect2(Vector2f(BLOC, 4*BLOC));
    rect2.setFillColor(Color(163,73,164));
    //rect2.setPosition(21*BLOC, 11*BLOC);
    RectangleShape rect3(Vector2f(BLOC, 4*BLOC));
    rect3.setFillColor(Color::Yellow);
    //rect3.setPosition(19*BLOC, 6*BLOC);

    //on dessine les boutons
    RectangleShape bouton1(Vector2f(BLOC, BLOC));
    bouton1.setFillColor(Color::Red);
    bouton1.setPosition(19*BLOC, 26*BLOC);
    RectangleShape bouton2(Vector2f(BLOC, BLOC));
    bouton2.setFillColor(Color(163,73,164));
    bouton2.setPosition(13*BLOC, 19*BLOC);
    RectangleShape bouton3(Vector2f(BLOC, BLOC));
    bouton3.setFillColor(Color::Yellow);
    bouton3.setPosition(23*BLOC, 8*BLOC);

    Position avant;

    //d�but du jeu
    while (fenetre.isOpen())
    {

        Position porte1;
        porte1.x = 14*BLOC;
        porte1.y = 26 * BLOC;
        Position porte2;
        porte2.x = 21*BLOC;
        porte2.y = 11*BLOC;
        Position porte3;
        porte3.x = 19*BLOC;
        porte3.y =6*BLOC;




        Event event;
        while (fenetre.pollEvent(event))
        {

            switch (event.type)
            {
            case Event::Closed:
                fenetre.close();
                break;
            }


            //on d�finit la position du perso en bloc
            Position blocPerso;
            blocPerso.x = posPerso.x / NB_BLOC_LARGEUR+2;
            blocPerso.y = posPerso.y / NB_BLOC_HAUTEUR;

            avant.x = posPerso.x;
            avant.y = posPerso.y;

            if (Keyboard::isKeyPressed(Keyboard::Escape))
                fenetre.close();
            if (Keyboard::isKeyPressed(Keyboard::Up))
                posPerso.y-=VITESSE;
            if (Keyboard::isKeyPressed(Keyboard::Down))
                posPerso.y+=VITESSE;
            if (Keyboard::isKeyPressed(Keyboard::Left))
            {
                if (posPerso.x==(porte1.x+BLOC) && (posPerso.y+2*BLOC)>porte1.y )
                    break;
                if (posPerso.x==(porte2.x+BLOC) && (posPerso.y+2*BLOC)<(porte2.y+4*BLOC +5) && (posPerso.y)>porte2.y )
                    break;
                if (posPerso.x==(porte3.x+BLOC) && (posPerso.y+2*BLOC)<(porte3.y+4*BLOC +5) && (posPerso.y)>porte3.y )
                    break;
                posPerso.x-=VITESSE;
            }

            if (Keyboard::isKeyPressed(Keyboard::Right))
            {
                if ((posPerso.x+BLOC)==porte1.x && (posPerso.y+2*BLOC)>=porte1.y+5)
                    break;
                if ((posPerso.x+BLOC)==porte2.x && (posPerso.y+2*BLOC)<(porte2.y+4*BLOC +5) && (posPerso.y)>porte2.y )
                    break;
                if ((posPerso.x+BLOC)==porte3.x && (posPerso.y+2*BLOC)<(porte3.y+4*BLOC +5) && (posPerso.y)>porte3.y )
                    break;
                posPerso.x+=VITESSE;
            }

            for (int b=0; b<NB_BLOC_HAUTEUR; b++)
            {
                for (int a=0; a<NB_BLOC_LARGEUR; a++)
                {
                    int top = b*BLOC-BLOC/2 - BLOC/6;
                    int bottom = b*BLOC+BLOC;
                    int left = a * BLOC;
                    int right = a * BLOC + BLOC;

                    for(int numerocase = 1; numerocase < 27; numerocase++)
                    {
                        if (numerocase==6)
                            numerocase+=3;
                        if (numerocase==7)
                            numerocase+=2;
                        if (numerocase==8)
                            numerocase++;


                        if (carte[b][a] == numerocase && posPerso.x + BLOC >= left && posPerso.x <= right && posPerso.y + BLOC >= top && posPerso.y<=bottom)
                        {
                            posPerso.x = avant.x;
                            posPerso.y = avant.y;
                        }
                    }

                }
            }

        }

        //on dessine la map
        for (int k=0; k<NB_BLOC_HAUTEUR; k++)
        {
            for (int l=0; l<NB_BLOC_LARGEUR; l++)
            {
                fenetre.draw(blocTileset[k][l]);
            }
        }



        FloatRect boxperso = perso.getGlobalBounds();



        //bouton
        FloatRect boxbutton1 = bouton1.getGlobalBounds();
        FloatRect boxbutton2 = bouton2.getGlobalBounds();
        FloatRect boxbutton3 = bouton3.getGlobalBounds();

        if (boxperso.intersects(boxbutton1))
            porte1.y = porte1.y - 2* BLOC;
        if (boxperso.intersects(boxbutton2))
            porte2.y = porte2.y + 4* BLOC;
        if (boxperso.intersects(boxbutton3))
            porte3.y = porte3.y - 4* BLOC;



        //on place et on dessine le perso
        perso.setPosition(posPerso.x,posPerso.y);
        fenetre.draw(perso);

        rect1.setPosition(porte1.x, porte1.y);
        rect2.setPosition(porte2.x, porte2.y);
        rect3.setPosition(porte3.x, porte3.y);
        fenetre.draw(bloc1);
        fenetre.draw(rect1);
        fenetre.draw(rect2);
        fenetre.draw(rect3);
        fenetre.draw(bouton1);
        fenetre.draw(bouton2);
        fenetre.draw(bouton3);
        fenetre.display();
    }
    return 0;
}



